# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the lomiri-settings-components package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-settings-components\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-29 21:15+0700\n"
"PO-Revision-Date: 2023-04-13 05:14+0000\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <https://hosted.weblate.org/projects/lomiri/lomiri-"
"settings-components/ka/>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.17-dev\n"

#: plugins/Lomiri/Settings/Components/Calendar.qml:232
msgctxt "Header text: keep it short and upper case"
msgid "WEEK"
msgstr "კვირა"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprint.qml:82
msgid "Fingerprint Name"
msgstr "ანაბეჭდის სახელი"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprint.qml:109
msgid "Delete Fingerprint"
msgstr "ანაბეჭდის წაშლა"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprint.qml:126
msgid "Sorry, the fingerprint could not be deleted."
msgstr ""

#: plugins/Lomiri/Settings/Fingerprint/Fingerprint.qml:131
#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:362
#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:139
#: plugins/Lomiri/Settings/Vpn/VpnEditor.qml:149
msgid "OK"
msgstr "დიახ"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:32
msgid "Fingerprint ID"
msgstr "ანაბეჭდის ID"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:93
#, qt-format
msgid "Finger %1"
msgstr "თითი %1"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:212
msgid "Passcode required"
msgstr "კოდი აუცილებელია"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:216
msgid "You must set a passcode to use fingerprint ID"
msgstr ""

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:223
msgid "Set Passcode…"
msgstr "კოდის დაყენება…"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:299
msgid "Add fingerprint"
msgstr "ანაბეჭდის დამატება"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:312
msgid "Remove All…"
msgstr "ყველას წაშლა…"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:327
msgid "Are you sure you want to forget all stored fingerprints?"
msgstr ""

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:336
#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:356
#: plugins/Lomiri/Settings/Vpn/DialogFile.qml:165
#: plugins/Lomiri/Settings/Vpn/VpnEditor.qml:136
#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:104
msgid "Cancel"
msgstr "გაუქმება"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:343
#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:84
msgid "Remove"
msgstr "წაშლა"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:357
#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:104
msgid "Sorry, the reader doesn’t seem to be working."
msgstr ""

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:62
msgid "Tap your finger repeatedly on the reader."
msgstr ""

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:79
msgid "Keep your finger on the reader for longer."
msgstr ""

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:95
msgid "Back"
msgstr "უკან"

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:132
msgid "All done!"
msgstr "ყველაფერი მზადაა!"

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:298
msgid "Swipe your finger over the reader."
msgstr ""

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:312
#, qt-format
msgid "%1%"
msgstr "%1%"

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:381
msgid "Next"
msgstr "შემდეგი"

#: plugins/Lomiri/Settings/Menus/CalendarMenu.qml:59
#, qt-format
msgctxt "%1=month name, %2=4-digit year"
msgid "%1 %2"
msgstr "%1 %2"

#: plugins/Lomiri/Settings/Menus/ModemInfoItem.qml:102
msgid "Roaming"
msgstr "როუმინგი"

#: plugins/Lomiri/Settings/Menus/ModemInfoItem.qml:127
msgid "Unlock SIM"
msgstr "SIM-ის განბლოკვა"

#: plugins/Lomiri/Settings/Menus/SnapDecisionMenu.qml:64
msgid "Message"
msgstr "შეტყობინება"

#: plugins/Lomiri/Settings/Menus/SnapDecisionMenu.qml:76
msgid "Call back"
msgstr "გადარეკვა"

#: plugins/Lomiri/Settings/Menus/SnapDecisionMenu.qml:93
#: plugins/Lomiri/Settings/Menus/TextMessageMenu.qml:38
msgid "Send"
msgstr "გაგზავნა"

#: plugins/Lomiri/Settings/Vpn/DialogFile.qml:173
msgid "Accept"
msgstr "დასტური"

#: plugins/Lomiri/Settings/Vpn/FileSelector.qml:25
msgid "Choose…"
msgstr "არჩევა…"

#: plugins/Lomiri/Settings/Vpn/FileSelector.qml:34
#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:431
#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:513
msgid "None"
msgstr "არაფერი"

#: plugins/Lomiri/Settings/Vpn/Openvpn/AuthTypeField.qml:29
msgid "Authentication type:"
msgstr "ავთენტიფიკაციის ტიპი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/AuthTypeField.qml:42
msgid "Certificates (TLS)"
msgstr "სერტიფიკატები (TLS)"

#: plugins/Lomiri/Settings/Vpn/Openvpn/AuthTypeField.qml:43
msgid "Password"
msgstr "პაროლი"

#: plugins/Lomiri/Settings/Vpn/Openvpn/AuthTypeField.qml:44
msgid "Password with certificates (TLS)"
msgstr "პაროლი სერთიფიკატებით (TLS)"

#: plugins/Lomiri/Settings/Vpn/Openvpn/AuthTypeField.qml:45
msgid "Static key"
msgstr "სტატიკური გასაღები"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:123
#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:114
msgid "Server:"
msgstr "სერვერი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:137
msgid "Port:"
msgstr "პორტი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:187
msgid "Use custom gateway port"
msgstr ""

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:215
msgid "Protocol:"
msgstr "ოქმი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:304
msgid "Username:"
msgstr "სახელი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:319
#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:165
msgid "Password:"
msgstr "პაროლი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:341
msgid "Client certificate:"
msgstr "კლიენტის სერტიფიკატი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:349
#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:396
msgid "Choose Certificate…"
msgstr "სერტიფიკატის არჩევა…"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:356
msgid "Private key:"
msgstr "პირადი გასაღები:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:364
#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:417
#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:497
msgid "Choose Key…"
msgstr "აირჩიეთ გასაღები…"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:371
msgid "Key password:"
msgstr "გასაღების პაროლი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:388
msgid "CA certificate:"
msgstr "CA სერტიფიკატი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:409
msgid "Static key:"
msgstr "სტატიკური გასაღები:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:421
#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:502
msgid "Key direction:"
msgstr "გასაღების მიმართულება:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:439
msgid "Remote IP:"
msgstr "დაშორებული IP მისამართი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:454
msgid "Local IP:"
msgstr "ლოკალური IP მისამართი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:479
msgid "Use additional TLS authentication:"
msgstr "დამატებითი TLS ავთენტიკაციის გამოყენება:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:488
msgid "TLS key:"
msgstr "TLS გასაღები:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:530
msgid "Verify peer certificate:"
msgstr "პარტნიორის სერტიფიკატის გადამოწმება:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:539
msgid "Peer certificate TLS type:"
msgstr "პარტნიორის სერტიფიკატის TLS ტიპი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:547
msgid "Server"
msgstr "სერვერი"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:548
msgid "Client"
msgstr "კლიენტი"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:558
msgid "Cipher:"
msgstr "შიფრი:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:565
msgid "Default"
msgstr "ნაგულისხმები"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:595
msgid "Compress data"
msgstr "მონაცემების შეკუმშვა"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:150
msgid "User:"
msgstr "მომხმარებელი:"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:180
msgid "NT Domain:"
msgstr "NT დომენი:"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:192
msgid "Authentication methods:"
msgstr "ავთენტიკაციის მეთოდები:"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:292
msgid "Use Point-to-Point encryption"
msgstr "წერტილიდან წერტილამდე შიფრაცია"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:301
msgid "All Available (Default)"
msgstr "ყველა ხელმისაწვდომი (ნაგულისხმები)"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:302
msgid "128-bit (most secure)"
msgstr "128-ბიტიანი (ყველაზე დაცული)"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:303
msgid "40-bit (less secure)"
msgstr "40-ბიტიანი (ნაკლებად დაცული)"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:319
msgid "Allow stateful encryption"
msgstr "სრული მიერთების დაშიფვრის გამოყენება"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:333
msgid "Allow BSD data compression"
msgstr "მონაცემების BSD შეკუმშვა"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:347
msgid "Allow Deflate data compression"
msgstr "მონაცემების Deflate შეკუმშვა"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:361
msgid "Use TCP Header compression"
msgstr "TCP თავსართების შეკუმშვა"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:375
msgid "Send PPP echo packets"
msgstr "PPP ექოს გაგზავნა"

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithDns.qml:27
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithoutDns.qml:27
msgid "You’re using this VPN for all Internet traffic."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithDns.qml:33
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml:39
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml:39
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SetUpUnused.qml:40
msgid ""
"Your Wi-Fi/mobile provider can see when and how much you use the Internet, "
"but not what for."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithDns.qml:39
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithoutDns.qml:45
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml:51
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml:51
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SetUpUnused.qml:47
msgid "The VPN provider can see or modify your Internet traffic."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithDns.qml:45
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithoutDns.qml:51
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml:57
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml:57
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SetUpUnused.qml:54
msgid "Web sites and other service providers can still monitor your use."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithoutDns.qml:33
msgid ""
"Your Wi-Fi/mobile provider can still see when and how much you use the "
"Internet."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithoutDns.qml:39
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml:45
msgid "The DNS provider can see which Web sites and other services you use."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:30
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NoCert.qml:27
msgid "This VPN is not safe to use."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:36
msgid ""
"The server certificate is not valid. The VPN provider may be being "
"impersonated."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:46
#, qt-format
msgid "Details: %1"
msgstr "დეტალები: %1"

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:51
msgid "The certificate was not found."
msgstr "სერტიფიკატი ვერ ვიპოვე."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:54
msgid "The certificate is empty."
msgstr "სერტიფიკატი ცარიელია."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:57
msgid "The certificate is self signed."
msgstr "სერტიფიკატი თვითხელოწერილია."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:60
msgid "The certificate has expired."
msgstr "სერტიფიკატი ვადაგასულია."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:63
msgid "The certificate is blacklisted."
msgstr "სერტიფიკატი შავ სიაშია."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NoCert.qml:33
msgid ""
"It does not provide a certificate. The VPN provider could be impersonated."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml:27
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml:27
msgid "This VPN configuration is not installed."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml:33
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml:33
msgid "If you install it:"
msgstr "თუ დააყენებთ:"

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml:45
msgid "The DNS provider can see which Web sites and other services you use. "
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SetUpUnused.qml:27
msgid "This VPN is set up, but not in use now."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SetUpUnused.qml:33
msgid "When you use it:"
msgstr "როდის გამოიყენებთ:"

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SomeTraffic.qml:27
msgid "You’re using this VPN for specific services."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SomeTraffic.qml:33
msgid "Your traffic to these services is private to them and the VPN provider."
msgstr ""

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SomeTraffic.qml:39
msgid "Your Wi-Fi/mobile provider can track your use of any other services. "
msgstr ""

#: plugins/Lomiri/Settings/Vpn/VpnEditor.qml:25
msgid "Set up VPN"
msgstr "VPN-ის მორგება"

#: plugins/Lomiri/Settings/Vpn/VpnList.qml:63
msgid "Delete configuration"
msgstr "კონფიგურაციის წაშლა"

#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:53
#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:58
#, qt-format
msgid "VPN “%1”"
msgstr "VPN “%1”"

#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:69
msgid "VPN"
msgstr "VPN"

#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:95
msgid "Change"
msgstr "ცვლილება"

#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:112
msgid "Install"
msgstr "დაყენება"

#: plugins/Lomiri/Settings/Vpn/VpnRoutesField.qml:30
msgid "Use this VPN for:"
msgstr ""

#: plugins/Lomiri/Settings/Vpn/VpnRoutesField.qml:60
msgid "All network connections"
msgstr "ყველა ქსელური შეერთება"

#: plugins/Lomiri/Settings/Vpn/VpnRoutesField.qml:76
msgid "Its own network"
msgstr "მისი საკუთარი ქსელი"

#: plugins/Lomiri/Settings/Vpn/VpnTypeField.qml:32
msgid "Type:"
msgstr "ტიპი:"

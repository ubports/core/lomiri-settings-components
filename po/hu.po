# Hungarian translation for lomiri-settings-components
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the lomiri-settings-components package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-settings-components\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-29 21:15+0700\n"
"PO-Revision-Date: 2023-10-18 05:54+0000\n"
"Last-Translator: Lundrin <berenyi.vilmos@tutanota.com>\n"
"Language-Team: Hungarian <https://hosted.weblate.org/projects/lomiri/lomiri-"
"settings-components/hu/>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.1\n"
"X-Launchpad-Export-Date: 2016-05-27 05:36+0000\n"

#: plugins/Lomiri/Settings/Components/Calendar.qml:232
msgctxt "Header text: keep it short and upper case"
msgid "WEEK"
msgstr "HÉT"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprint.qml:82
msgid "Fingerprint Name"
msgstr "Ujjlenyomat neve"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprint.qml:109
msgid "Delete Fingerprint"
msgstr "Ujjlenyomat törlése"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprint.qml:126
msgid "Sorry, the fingerprint could not be deleted."
msgstr "Az ujjlenyomat törlése sikertelen."

#: plugins/Lomiri/Settings/Fingerprint/Fingerprint.qml:131
#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:362
#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:139
#: plugins/Lomiri/Settings/Vpn/VpnEditor.qml:149
msgid "OK"
msgstr "OK"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:32
msgid "Fingerprint ID"
msgstr "Ujjlenyomat azonosító"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:93
#, qt-format
msgid "Finger %1"
msgstr "Ujj %1"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:212
msgid "Passcode required"
msgstr "Jelkód szükséges"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:216
msgid "You must set a passcode to use fingerprint ID"
msgstr "Be kell állítania egy jelkódot az ujjlenyomat azonosítás használatához"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:223
msgid "Set Passcode…"
msgstr "Jelkód beállítása…"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:299
msgid "Add fingerprint"
msgstr "Ujjlenyomat hozzáadása"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:312
msgid "Remove All…"
msgstr "Összes eltávolítása…"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:327
msgid "Are you sure you want to forget all stored fingerprints?"
msgstr "Biztosan törölni akarja az összes tárolt ujjlenyomatot?"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:336
#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:356
#: plugins/Lomiri/Settings/Vpn/DialogFile.qml:165
#: plugins/Lomiri/Settings/Vpn/VpnEditor.qml:136
#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:104
msgid "Cancel"
msgstr "Mégsem"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:343
#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:84
msgid "Remove"
msgstr "Eltávolítás"

#: plugins/Lomiri/Settings/Fingerprint/Fingerprints.qml:357
#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:104
msgid "Sorry, the reader doesn’t seem to be working."
msgstr "Sajnáljuk, úgy tűnik, nem működik az ujjlenyomat olvasó."

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:62
msgid "Tap your finger repeatedly on the reader."
msgstr "Érintse többször az ujját az olvasóhoz."

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:79
msgid "Keep your finger on the reader for longer."
msgstr "Tartsa az ujját az olvasón hosszabb időre."

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:95
msgid "Back"
msgstr "Vissza"

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:132
msgid "All done!"
msgstr "Minden kész!"

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:298
msgid "Swipe your finger over the reader."
msgstr "Húzza el az ujját az olvasón."

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:312
#, qt-format
msgid "%1%"
msgstr "%1%"

#: plugins/Lomiri/Settings/Fingerprint/Setup.qml:381
msgid "Next"
msgstr "Köv."

#: plugins/Lomiri/Settings/Menus/CalendarMenu.qml:59
#, qt-format
msgctxt "%1=month name, %2=4-digit year"
msgid "%1 %2"
msgstr "%2 %1"

#: plugins/Lomiri/Settings/Menus/ModemInfoItem.qml:102
msgid "Roaming"
msgstr "Barangolás"

#: plugins/Lomiri/Settings/Menus/ModemInfoItem.qml:127
msgid "Unlock SIM"
msgstr "SIM feloldása"

#: plugins/Lomiri/Settings/Menus/SnapDecisionMenu.qml:64
msgid "Message"
msgstr "Üzenet"

#: plugins/Lomiri/Settings/Menus/SnapDecisionMenu.qml:76
msgid "Call back"
msgstr "Visszahívás"

#: plugins/Lomiri/Settings/Menus/SnapDecisionMenu.qml:93
#: plugins/Lomiri/Settings/Menus/TextMessageMenu.qml:38
msgid "Send"
msgstr "Küldés"

#: plugins/Lomiri/Settings/Vpn/DialogFile.qml:173
msgid "Accept"
msgstr "Elfogadás"

#: plugins/Lomiri/Settings/Vpn/FileSelector.qml:25
msgid "Choose…"
msgstr "Válasszon…"

#: plugins/Lomiri/Settings/Vpn/FileSelector.qml:34
#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:431
#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:513
msgid "None"
msgstr "Nincs"

#: plugins/Lomiri/Settings/Vpn/Openvpn/AuthTypeField.qml:29
msgid "Authentication type:"
msgstr "Hitelesítés típusa:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/AuthTypeField.qml:42
msgid "Certificates (TLS)"
msgstr "Tanúsítvány (TLS)"

#: plugins/Lomiri/Settings/Vpn/Openvpn/AuthTypeField.qml:43
msgid "Password"
msgstr "Jelszó"

#: plugins/Lomiri/Settings/Vpn/Openvpn/AuthTypeField.qml:44
msgid "Password with certificates (TLS)"
msgstr "Jelszó tanúsítvánnyal (TLS)"

#: plugins/Lomiri/Settings/Vpn/Openvpn/AuthTypeField.qml:45
msgid "Static key"
msgstr "Statikus kulcs"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:123
#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:114
msgid "Server:"
msgstr "Kiszolgáló:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:137
msgid "Port:"
msgstr "Port:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:187
msgid "Use custom gateway port"
msgstr "Egyéni átjáró port használata"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:215
msgid "Protocol:"
msgstr "Protokoll:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:304
msgid "Username:"
msgstr "Felhasználónév:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:319
#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:165
msgid "Password:"
msgstr "Jelszó:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:341
msgid "Client certificate:"
msgstr "Kliens tanúsítvány:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:349
#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:396
msgid "Choose Certificate…"
msgstr "Tanúsítvány kiválasztása…"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:356
msgid "Private key:"
msgstr "Személyes kulcs:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:364
#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:417
#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:497
msgid "Choose Key…"
msgstr "Kulcs kiválasztása…"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:371
msgid "Key password:"
msgstr "Kulcs jelszava:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:388
msgid "CA certificate:"
msgstr "CA tanúsítvány:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:409
msgid "Static key:"
msgstr "Statikus kulcs:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:421
#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:502
msgid "Key direction:"
msgstr "Kulcsirány:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:439
msgid "Remote IP:"
msgstr "Távoli IP:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:454
msgid "Local IP:"
msgstr "Lokális IP:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:479
msgid "Use additional TLS authentication:"
msgstr "TLS hitelesítés használta:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:488
msgid "TLS key:"
msgstr "TLS kulcs:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:530
msgid "Verify peer certificate:"
msgstr "Partner tanúsítványának ellenőrzése:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:539
msgid "Peer certificate TLS type:"
msgstr "Partner tanúsítvány TLS típusa:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:547
msgid "Server"
msgstr "Kiszolgáló"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:548
msgid "Client"
msgstr "Kliens"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:558
msgid "Cipher:"
msgstr "Rejtjelező:"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:565
msgid "Default"
msgstr "Alapértelmezett"

#: plugins/Lomiri/Settings/Vpn/Openvpn/Editor.qml:595
msgid "Compress data"
msgstr "Adat tömörítése"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:150
msgid "User:"
msgstr "Felhasználó:"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:180
msgid "NT Domain:"
msgstr "NT tartomány:"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:192
msgid "Authentication methods:"
msgstr "Hitelesítés módszere:"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:292
msgid "Use Point-to-Point encryption"
msgstr "Pontól pontig titkosítás"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:301
msgid "All Available (Default)"
msgstr "Minden elérhető (alapértelmezett)"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:302
msgid "128-bit (most secure)"
msgstr "128 bites (legbiztonságosabb)"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:303
msgid "40-bit (less secure)"
msgstr "40 bites (kevésbé biztonságos)"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:319
msgid "Allow stateful encryption"
msgstr "Állapotmentes titkosítás engedélyezése"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:333
msgid "Allow BSD data compression"
msgstr "BSD adattömörítés engedélyezése"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:347
msgid "Allow Deflate data compression"
msgstr "Deflate adattömörítés engedélyezése"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:361
msgid "Use TCP Header compression"
msgstr "TCP fejléc tömörítés használata"

#: plugins/Lomiri/Settings/Vpn/Pptp/Editor.qml:375
msgid "Send PPP echo packets"
msgstr "PPP visszhangcsomagok küldése"

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithDns.qml:27
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithoutDns.qml:27
msgid "You’re using this VPN for all Internet traffic."
msgstr "Ezt a VPN-t használja az összes internetes forgalomhoz."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithDns.qml:33
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml:39
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml:39
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SetUpUnused.qml:40
msgid ""
"Your Wi-Fi/mobile provider can see when and how much you use the Internet, "
"but not what for."
msgstr ""
"A Wi-Fi/mobil internet szolgáltatója látja, hogy mikor és mennyit használja "
"az internetet, de azt nem, hogy mire."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithDns.qml:39
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithoutDns.qml:45
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml:51
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml:51
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SetUpUnused.qml:47
msgid "The VPN provider can see or modify your Internet traffic."
msgstr ""
"A VPN szolgáltató látja, illetve módosíthatja az Ön internetes forgalmát."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithDns.qml:45
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithoutDns.qml:51
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml:57
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml:57
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SetUpUnused.qml:54
msgid "Web sites and other service providers can still monitor your use."
msgstr ""
"A weboldal és más szolgáltatás üzemeltetők még mindig nyomon követhetik a "
"használati szokásait."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithoutDns.qml:33
msgid ""
"Your Wi-Fi/mobile provider can still see when and how much you use the "
"Internet."
msgstr ""
"A Wi-Fi/mobil internet szolgáltatója még mindig látja, hogy mikor és mennyit "
"használja az internetet."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/AllTrafficWithoutDns.qml:39
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml:45
msgid "The DNS provider can see which Web sites and other services you use."
msgstr ""
"A DNS szolgáltató láthatja, hogy mely weboldalakat látogatja és mely "
"szolgáltatásokat használja."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:30
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NoCert.qml:27
msgid "This VPN is not safe to use."
msgstr "Ennek a VPN-nek nem biztonságos a használata."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:36
msgid ""
"The server certificate is not valid. The VPN provider may be being "
"impersonated."
msgstr ""
"A kiszolgáló tanúsítványa nem érvényes. Lehet, hogy a VPN szolgáltatója "
"„megszemélyesítéses” támadás áldozat lett."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:46
#, qt-format
msgid "Details: %1"
msgstr "Részletek: %1"

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:51
msgid "The certificate was not found."
msgstr "A tanúsítvány nem található."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:54
msgid "The certificate is empty."
msgstr "A tanúsítvány üres."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:57
msgid "The certificate is self signed."
msgstr "Ez egy önaláírt tanúsítvány."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:60
msgid "The certificate has expired."
msgstr "A tanúsítvány lejárt."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/InvalidCert.qml:63
msgid "The certificate is blacklisted."
msgstr "A tanúsítvány tiltólistán van."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NoCert.qml:33
msgid ""
"It does not provide a certificate. The VPN provider could be impersonated."
msgstr ""
"A kiszolgáló nem nyújt semmilyen tanúsítványt. Lehet, hogy a VPN "
"szolgáltatója „megszemélyesítéses” támadás áldozat lett."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml:27
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml:27
msgid "This VPN configuration is not installed."
msgstr "A VPN konfiguráció nincs telepítve."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml:33
#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithRoutes.qml:33
msgid "If you install it:"
msgstr "Ha feltelepíti:"

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/NotInstalledWithoutRoutes.qml:45
msgid "The DNS provider can see which Web sites and other services you use. "
msgstr ""
"A DNS szolgáltató látni fogja, hogy mely weboldalakat látogatja és mely "
"szolgáltatásokat használja. "

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SetUpUnused.qml:27
msgid "This VPN is set up, but not in use now."
msgstr "A VPN be van állítva, de nincs használatban."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SetUpUnused.qml:33
msgid "When you use it:"
msgstr "Ha használja:"

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SomeTraffic.qml:27
msgid "You’re using this VPN for specific services."
msgstr "A VPN-t csak néhány megadott szolgáltatásnál használja."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SomeTraffic.qml:33
msgid "Your traffic to these services is private to them and the VPN provider."
msgstr ""
"Az ezen szolgáltatások és az Ön között zajló forgalom csak Ön és a VPN "
"szolgáltatója számára elérhető."

#: plugins/Lomiri/Settings/Vpn/PreviewDialog/SomeTraffic.qml:39
msgid "Your Wi-Fi/mobile provider can track your use of any other services. "
msgstr ""
"A Wi-Fi/mobil internet szolgáltatója minden más szolgáltatás használatát "
"nyomon tudja követni. "

#: plugins/Lomiri/Settings/Vpn/VpnEditor.qml:25
msgid "Set up VPN"
msgstr "VPN beállítása"

#: plugins/Lomiri/Settings/Vpn/VpnList.qml:63
msgid "Delete configuration"
msgstr "Konfiguráció törlése"

#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:53
#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:58
#, qt-format
msgid "VPN “%1”"
msgstr "„%1” VPN"

#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:69
msgid "VPN"
msgstr "VPN"

#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:95
msgid "Change"
msgstr "Módosítás"

#: plugins/Lomiri/Settings/Vpn/VpnPreviewDialog.qml:112
msgid "Install"
msgstr "Telepítés"

#: plugins/Lomiri/Settings/Vpn/VpnRoutesField.qml:30
msgid "Use this VPN for:"
msgstr "VPN használata ehhez:"

#: plugins/Lomiri/Settings/Vpn/VpnRoutesField.qml:60
msgid "All network connections"
msgstr "Minden hálózati kapcsolathoz"

#: plugins/Lomiri/Settings/Vpn/VpnRoutesField.qml:76
msgid "Its own network"
msgstr "Csak a saját hálózatához"

#: plugins/Lomiri/Settings/Vpn/VpnTypeField.qml:32
msgid "Type:"
msgstr "Típus:"

#~ msgid "Quick reply with:"
#~ msgstr "Gyorsválasz:"
